const mongoose = require('mongoose');
const JuiceRecipe = require('./models/juiceRecipe');
const juiceRecipeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  ingredients: {
    type: [String],
    required: true
  },
  instructions: {
    type: String,
    required: true
  },
  created_at: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('JuiceRecipe', juiceRecipeSchema);