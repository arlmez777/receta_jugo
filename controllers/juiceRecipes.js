const JuiceRecipe = require('../models/juiceRecipes');

const getAllRecipes = (req, res) => {
  Recipe.find({}, (err, recipes) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.json(recipes);
    }
  });
};

const getRecipeById = (req, res) => {
  Recipe.findById(req.params.id, (err, recipe) => {
    if (err) {
      res.status(500).send(err);
    } else if (!recipe) {
      res.status(404).send('Recipe not found');
    } else {
      res.json(recipe);
    }
  });
};

const createRecipe = (req, res) => {
  const recipe = new Recipe(req.body);
  recipe.save((err) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(201).json(recipe);
    }
  });
};

const updateRecipe = (req, res) => {
  Recipe.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, recipe) => {
    if (err) {
      res.status(500).send(err);
    } else if (!recipe) {
      res.status(404).send('Recipe not found');
    } else {
      res.json(recipe);
    }
  });
};

const deleteRecipe = (req, res) => {
  Recipe.findByIdAndDelete(req.params.id, (err, recipe) => {
    if (err) {
      res.status(500).send(err);
    } else if (!recipe) {
      res.status(404).send('Recipe not found');
    } else {
      res.sendStatus(204);
    }
  });
};

module.exports = {
  getAllRecipes,
  getRecipeById,
  createRecipe,
  updateRecipe,
  deleteRecipe,
};